# How It Works

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

Our device is actually, in many ways, shockingly simple. It has lights (LEDs - light emitting diodes) that emit light at very specific wavelengths, which then bounce off objects like carrots, or carrot pulp, or spinach, or soil. Some of it is absorbed by the object and turned into other forms of energy, like heat. A light sensor in the device reads how much light bounces back for each wavelength very quickly and multiple times throughout a given measurement.

![image](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/how-it-works.png)

Light bouncing off matters because this is a characteristic of objects which directly correlates to the chemical compounds making up that object. So in the case of food and agriculture, there are known correlations between light reflectance at specific wavelengths and the amount of different nutrients found in food (vitamins, antioxidants, and aromatic compounds, to name a few), the levels of organic carbon in soil, and chlorophyll content in plants, etc. 

What makes this extremely complicated is that these light-bouncing characteristics and compounds “overlap” with each other — so we need to look at lots of data to try to parse out what is causing the response we’re seeing.

**This is why we’re working with beta users to collect data for calibration purposes, before making this a commercial device.**
