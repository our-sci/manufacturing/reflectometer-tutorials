# Connecting Firmware

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

**Developer Info Coming Soon!!**

In the meantime:

<p>
Post your questions on <a href="http://forum.goatech.org/">the Our Sci forum</a>. This will make sure we can find the best person to support you! 
</p>

<p>
Or, reach out to hardware-support@our-sci.net to ask your question over email, or schedule a meeting at a time that works for you.
</p>
<br>
