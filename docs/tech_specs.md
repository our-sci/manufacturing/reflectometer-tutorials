# Tech Specs

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

The Meter is a robust, general purpose, ten wavelength device for measuring spectral reflectance from liquids (e.g. milk), flat objects (e.g. leaves) or solids (e.g. soils, produce).

The circuit has an on-board lithium battery charger (designed for a 3.7V 18650 Li battery), current-controlled pulsed LEDs to illuminate the sample, as well as an optional hall effect sensor, contactless temperature sensor and temp/pressure/rh sensor (BME280).

<details>

<summary><b>Key Features</b></summary>
<br>

<ul>
<li>10-wavelength output not affected by ambient light</li>
<li>Robust and field-ready</li>
<li>Connects to Survey Stack (Our-Sci's open-source software) to collect, share, and display results</li>
<li>Connects to any Android device wirelessly using Bluetooth or wired USB On The Go</li>
<li>Includes a wall charger and an internal 3400 mAh battery</li>
<li>Fully open source and modifiable</li>

</details>
<br>

<details>

<summary><b>Applications</b></summary>
<br>

<i>Designed for use with:</i>
<ul>
<li>liquids in 5 mL or 0.7 mL square cuvettes</li>
<li>liquid colormetric measurements (example: pH change)</li>
<li>cell density (940 nm)
    <ul>
    <li>examples: milk fat concentration; spectral properties of juices, wines, etc.</li>
    </ul>
<li>soil or other bulk solids</li>
<li>soil carbon estimation (requires calibration)</li>
<li>soil color estimation</li>
<li>solid objects (fruits, veggies)</li>
<li>spectral signatures for nutrient estimation (requires calibration)</li>
<li>leaves or other flat objects</li>
<li>NDVI (greenness) in leaves</li>
<li>relative carotenoids, anthocyanins, or other color-related compounds</li>

</details>
<br>

<details>

<summary><b>Default Output</b></summary>
<br>

<ul>
<li>Reflectance at all wavelengths (normalized from 0 - 10,000)</li>
<li>Chlorophyll fluoresence and any fluorescence with excitation <700 nm and flurorescence >700 nm</li>
<li>NDVI (greenness) in leaves</li>

</details>
<br>

<details>

<summary><b>Sensor Detail</b></summary>
<br>

<i>Reflectance hardware:</i>
<ul>
<li>10 wavelength (nm): UVB - 365; Visible - 385; 450; 500; 530; 587; 632; Far Red; NIR - 850; 880; 940</li>
<li>2 detectors (nm range): 300-700; 700-1100</li>

<br>

<i>Additional Sensors:</i>
<ul>
<li>Temperature (full range: -40 - 85 C, accuracy within 0 - 65 C: +/- 1C)</li>
<li>Relative Humidity (range: 0-100%, accuracy: +/-3%)</li>
<li>Pressure (range: 300-1100 hPa, accuracy: +/-0.25%)</li>
<li>VOCs (range: 0 - 500 IAQs, accuracy: +/- 15%)</li>
<li>Audio jack for additional inputs / outputs. Examples include soil senseors, moisture sensors, etc.</li>

</details>
<br>

<details>

<summary><b>Case</b></summary>
<br>

<ul>
<li>13.9 cm x 5.2 cm x 4.6 cm</li>
<li>3D printed PETG</li>
<li>Temperature resistant up to 60° C / 140° F</li>

</details>
<br>

<details>

<summary><b>Calibration</b></summary>
<br>

All instruments come individually calibrated to reduce instrument-to-instrument variation, as well as variaton by time and by temperature.
<br>
<br>
For long-term shifts in signal intensity, users may easily recalibrate. If you're working with several machines that you would like calibrated to the same standard, please contact hardware-support@our-sci.net.

</details> 
<br>

<h4>All of our products are open source</h4> 

To build this devices yourself, you will need the following documentation:

- [Firmware](https://gitlab.com/our-sci/reflectance-spec-firmware)

- [PCB](https://gitlab.com/our-sci/manufacturing/reflectance-spec-PCB)

- [CAD / Physical design](https://cad.onshape.com/documents/849be056da41993fee5440bf/w/5ea0968e39e13e6e3de3a5ba/e/f5bcf0fa9d4cdfc9de388d42)

- [Bill of Materials (everything but the PCB)](https://docs.google.com/spreadsheets/d/1d4Xplr8sztEgrEis-LiSqLnN9V1eaN1zSc19izoaaig/edit?usp=sharing)
