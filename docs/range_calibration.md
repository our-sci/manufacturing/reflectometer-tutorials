# Calibration Procedure

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

**If you are using the device in your own professional or research application, you should plan to run a calibration directly prior to running the unit.**

### Table of Contents
[TOC]


## Running a Calibration
The Reflectometer comes with a calibration standard that allows users to reset their devices reflectance range. By running calibrations often, the devices provide more consistent results over time and between different devices. Device-to-device comparability is further improved by using our normalization standards for common measurement objects (e.g. soils or liquids in a cuvette).


### When to Run a Calibration
* At the beginning of day when you intend to collect data.
* After a significant change in environmental conditions or temperature.
* NOTE: Calibrations should be run in the same environmental conditions, especially similar temperatures, to those in which you will be running your samples. In other words, don’t calibrate in a cold basement, and then go to a hot field and perform all of your measurements.

### How to Run a Calibration
In most cases, you will be running an existing survey that includes device measurements. These surveys should already include the calibration process, so you do not need to run a separate survey to calibrate your device.

If you are creating a survey that includes device measurements, please read the [Accessing the Calibration Procedure](#accessing-the-calibration-procedure) section below to learn how to embed the calibration process in your surveys.


### How to Calibrate
Most surveys that include measurements include question sets from the SurveyStack question set library. They will include a question asking if you have calibrated your device in the past 4 hours. If you answer no, the survey will walk you through the calibration process.

![calibrate_screenshot](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/calibration_screenshot.png)

Here are some important tips for a good calibration:

* Start by warming up the device. This is just a standard scan, that flashes each of the 10 LEDs a few times. The results for this scan do not matter since it's just a warmup.
> - After running a scan and returning to the survey, it may take a moment for the results to load. If after 20-30 seconds this screen still hasn't reported a result, you may need to refresh the page and run the warmup again. Make sure you are connected to wifi and your device is connected to the SurveyStack kit, then run the calibration again.
> > - NOTE: This tip is relevant for all measurements you run with the reflectometer.

* Always clean the surface of the instrument with a clean, non-scratchy cloth, like the one included in your kit.  A small amount of dust or sticky plant material will reflect light, impacting your calibration.

![clean_device](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/clean_device.png)

* Check the device clamp and make sure it’s not dirty.  If it is, you can clean it with a slightly moistened cloth.

* During the Open Blank step, open the clamp and aim the device into a relatively open space, as shown below. There should be nothing shiny or reflectant (e.g. windows or mirrors) in the area. We recommend pointing the device down a hallway or toward the corner of a room, as shown in the image.

![open_calibration](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/open_calibration.png)

* During the Cuvette Standard calibration step, start by unscrewing the Cuvette Standard from its stored location at the back of the device. 

* To position the 3D-printed cuvette standard, make sure it is facing the right direction and slide the lip under the clamp so that it lays flush against the LED window (There should be no large gaps between the Cuvette Standard and the instrument!). Make sure the window facing side is clean.

![cuvette_calibration](https://gitlab.com/our-sci/resources/-/raw/master/images/Reflectometer/Reflectometer%20Pics%207-21/old_and_new_cuvette_standard_example.PNG)

![image5](https://gitlab.com/our-sci/manufacturing/reflectometer-assembly/-/raw/master/misaligned_teflon.png "POOR ALIGNMENT OF TEFLON AGAINST SURFACE")
> **POOR CUVETTE ALIGNMENT SURFACE**

### Confirming Successsful Calibration

Once the calibration is complete and you have saved both the open blank and cuvette standard scans to device, you will be prompted to confirm the calibration was successful via a quality check. This simply requires that you repeat the Cuvette Standard measurement, again taking care to properly clean and position the cuvette. The Cuvette Standard confirmation should produce a notification of "Success" or "Error".

Here are some tips for confirming a range calibration:

* If the calibration was good, you will receive a "Success" notification. At this point, you can continue filling out the rest of the survey.  

* If you receive an error warning, that indicates that the calibration you just ran is not similar enough to that of the common manufacturer standard, which is run before devices are shipped out. Usually, there are two causes of error with the local cuvette standard calibration:

>> (1) The scan surface of the cuvette standard has visible inconsistencies.

>> (2) The physical positioning of the cuvette standard is not correct.

> To resolve the error, make sure that the cuvette standard sits flush against the surface, and the clamp magnets align with the magnets on the instrument. Also check for a clean scan surface on the cuvette standard and light guide. Once you've confirmed those checkpoints, click `RE-RUN` in the results tab so that you can redo the Cuvette Standard Quality Check. 
>> If you continue to run into "Error" warnings despite rescanning, you should restart the survey and try again.

### Re-doing a Calibration

To re-do a calibration, simply exit the current calibration and create a new survey, performing the calibration again until it is complete.

### Troubleshooting

_I can’t get my calibration to consistently fall within 2.5% of the common manufacturer calibration to yield a "Success" notification._

>  Usually, repeatable physical positioning of the Cuvette Standard against the instrument is the problem.  It should sit flush against the surface, and the clamp magnets should be lined up with the magnets on the instrument surface.  Also check for a clean surface.  Doing the same thing every time is the most important - so try to be repeatable!


### Accessing the Calibration Procedure
#### For survey creators
The simplest way to include the Reflectometer Calibration Process is to pull them into your survey from [SurveyStacks question set library](https://our-sci.gitlab.io/software/surveystack_tutorials/QSL/). You can add either of the following question sets into any survey you create within SurveyStack. 


- **'Reflectometer Calibration for Users'** is a question set that we embed at the beginning of all of our reflectometer-based surveys. 
> - If you are completing a reflectometer-based estimation survey you can run the calibration prior to your first measurement of the day, and then skip it for later samples.
> - If you use this question set you will need to add reflectometer measurement questions for your project.

- **'Simple Reflectometer Survey'** is a flexible question set that includes the 'Reflectometer Calibration for Users' question set and a space for three general reflectometer scans. This question set is a great drag-and-drop option to take reflectometer measurements for many use cases.
> - This question set is modifiable so you can select which objects to scan (ex: soil or leaves) and then add custom directions for your community.
> - It includes logic for skipping the calibration if the user has already calibrated their reflectometer that day. 

#### Stand-alone Calibration Survey
The [Device Calibration](https://app.surveystack.io/surveys/60c8f303e17d6c0001cc5b8c) survey is also available to users who want to calibrate their reflectometers in an isolated format, instead of embedded in your main survey.



## Technical Details: What a calibration does

**The calibration helps ensure a device is comparable to itself over time.**

The reflectometer uses 10 LEDs at 10 wavelengths in order to see how reflective the sample is at each wavelength.  The problem is that over time, the reflectivity for the exact same sample can drift over time.  Some reasons for this drift are:

* LEDs producing less light due to time or use
* The light detector changing over time
* Fading or scratching of the light guide which can change the amount of light hitting the sample or being received.
* Changes in the electronics which supply the power to drive the LEDs, causing the LED to produce more or less light.

In order to ensure that a single device is comparable over time, we calibrate that device to a standard set of objects for which the reflectance is not changing, and recalibrate the device to ensure those objects always return the same value.

In our case, we are calibrating to two objects: a blank space and a 3D printed white cuvette standard(highly reflective).  This device uses very brief pulses of light and subtracts off any background light - making ambient light sources effectively invisible to the instrument. As such, pointing the instrument into an empty room returns effectively zero detectable light back to the detector. This empty room functions as a cheap and universally accessible blank.

As such, **we assign a value of 0 to the results of the Open Blank measurement** for each wavelength.

The Cuvette Standard is universally reflective in the wavelength range we work in, so it is more or less the brightest thing we can find.

As such, **we assign a value of 10,000 to the results of the Common Manufacturer Cuvette Standard measurement that's run during manufacturing processes** for each wavelength. We then create a relative calibration between the Common Manufacturer Cuvette Standard and the individual Local Cuvette Standard sent out with each device.

So, even if the LEDs become weaker or there are scratches on the unit, we can be sure that objects will always report the about same reflective values because we have known low and high points of reference.

![image](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/calibration_explanation.png "Graphic of Calibration Process: Main Master Calibration -> Local Master Calibration -> User Calibration")

**What about comparability between devices?**

Device-to-device comparability is supported by calibration, because devices can always be traced back to their calibration standard, but it is not a full solution.  In order for all devices to be perfectly comparable over time, they need to be traceable back to a single, common standard. As a final manufacturer step, all devices are calibrated to a single common Cuvette Standard.

**If you need strong comparability between devices, you should post your specific application to the forum and we can discuss ways to get improved results.**

## Get Help

- Post your issues on the [Our Sci forum](http://forum.goatech.org/).  This will make sure we can find the best person to support you!

- Reach out to hardware-support@our-sci.net to ask your question over email, or schedule a meeting at a time that works for you.
