# Installing SurveyStack Kit

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

**Important Note: SurveyStack Kit is an Android App. SurveyStack Kit and Our Sci instruments require Android devices in order to connect and run measurements.**

## When to Install
**As is stated on the [Getting Started](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/getting_started/) page, the SurveyStack Kit app is only necessary for hardware integration. SurveyStack Kit is an entirely separate app from SurveyStack, and is meant only for hardware-integrated instruments such as the Our Sci Reflectometer or SAVR Kit.** 

> - If you are using SurveyStack with hardware integration, you will need the SurveyStack Kit app to connect with your instrument.
> - If you are using SurveyStack only as a surveying tool, and without any hardware integration, there is no need to install the SurveyStack Kit app.

### Downloading SurveyStack and SurveyStack Kit
<iframe width="560" height="315" src="https://www.youtube.com/embed/pTpuqH7D9ek" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
_This video covers Android phone setup. If you ordered an Android tablet with your meter, you can skip this step, because all apps are already pre-installed._

#### Two ways to download SurveyStack Kit
1. The SurveyStack Kit app can be installed from the Google PlayStore, by searching the <a href="https://play.google.com/store/apps/details?id=io.surveystack.app">SurveyStack Kit App</a> from Our Sci LLC.
2. You can install the SurveyStack Kit app within SurveyStack itself.

> - When you get to a survey that requires a  hardware integration to complete measurements (in either the SurveyStack App or the SurveyStack online browser), SurveyStack will prompt you to `Install Android App.`
> - When you see this pop up, you should press the button and select `+ Download APK` and then click `Install` when prompted.
> - Once the download is complete, you will see the app appear, and you can move it to your home screen if desired.
> - If you already have SurveyStack Kit installed, you can ignore the prompt and continute to run the measurement.


**Important Note**: Some Android phones have security restrictions related to downloading content from an unknown source online. You should go into `Settings` >> `Security` or `Permissions` and `Allow downloads from unknown sources on the internet` before you download the APK installer. If you do not do this first, errors may occur that require you to change these settings and restart the download / install process for SurveyStack Kit.

### Installing SurveyStack

If you expect to complete forms offline, you should download the SurveyStack App. For example, if you intend to complete surveys while soil sampling, without wifi or data available, you should download the app as instructed below. If you will only ever use this service will connected to Wifi, you can continue to use SurveyStack in a web browser if that is your preference.

<iframe class="embedded-content" height=450px src="https://our-sci.gitlab.io/software/surveystack_tutorials/download_app/"></iframe>

## Get Help

- Post your issues on the [Our Sci forum](http://forum.goatech.org/).  This will make sure we can find the best person to support you!

- Reach out to hardware-support@our-sci.net to ask your question over email, or schedule a meeting at a time that works for you.
