# Predicting Nutrient Density

## Why use the meter to measure nutrient density?
The meter is a handheld tool that allows for field measurement of nutrient density at a much lower price point than traditional benchtop analysis. It’s also noninvasive and nondestructive of the sample, and allows for a much greater range of predictions than something like a brix reflectometer. 

## Has this meter shown an ability to predict nutrient density?

Yes! In 2019, we reported that the meter, "combined with appropriately attainable metadata is just as effective at predicting nutrient quality as the much more expensive and complicated benchtop Siware device.” In 2020, we honed our prediction methods to improve predictive capacity and guard against overfitting. You can read more about our process and methods in our [2019](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/2019_report/#predicting-variation-with-spectroscopy) and [2020](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/2020%20Final%20Report/#results) reports. 


## Are there any public prediction models available?

We have predictive models ready for consumer use with the following 10 crops! 

| **Crop** | **Model 1** | **Model 2** | **Model 3** | **Model 4** |
| -------- | ----------- | ----------- | ----------- | ----------- |
| Beets | Antioxidants |
| Bok Choy | Antioxidants | Brix | Polyphenols |
| Carrots | Polyphenols |
| Kale | Brix | Polyphenols |
| Lettuce | Antioxidants | Polyphenols |
| Mustard Greens | Antioxidants | Brix | Polyphenols |
| Oats | Antioxidants | BQI | Polyphenols |
| Squash | Polyphenols |
| Swiss Chard | Antioxidants | Brix |
| Wheat | Antioxidants | BQI | Polyphenols | Proteins |
  


You can access these models in the [Nutrient Density Estimate survey](https://app.surveystack.io/groups/608d7e971c6a18000147f33a/surveys/60d38c7f6bc1da00013e9ba2/submissions/new) in SurveyStack.

## How can I get involved?

If you’re interested in partnering with this project to help build an open-source nutrient density database and library that will answer the question, *How do we grow the highest quality, tastiest, most nutrient-dense food?* take a look at the Bionutrient Institute website: [Get Involved.](https://www.bionutrientinstitute.org/getinvolved)

<br>

Testing samples, running them through the labs, correlating the data, and manufacturing the Bionutrient Meter – all of this requires resources. You can also donate to support the efforts of the BI [here](https://bionutrient.net/donate/). 