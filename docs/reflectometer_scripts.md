# Writing Surveys with Meter Scans

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

### Table of Contents
[TOC]

## Meter Scripts

You can use script questions to add custom javascript code to your survey. Using scripts you can reference anything in the survey's json object. This is helpful to display calculations or outcomes to the user and is also used for hardware integration.

### Script Question Type: Formatting

![image](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/Adding_a_script_to_a_survey.png "Script Question: blank --> completed for a standard reflectometer scan")

- Once you select the script question type within the SurveyStack builder, you will see the empty script page. 

- In order to create a script, you are required to choose a `Script Source` and fill in the `Parameters` (see the arrows in the image). *The "Reflectometer" script is the best script for running scans.*

- Optionally, you can choose to check the `Native Script` box so that the APK installer for SurveyStack Kit is visible on the script page (as is the case for the warmup question in the Device Calibration process).

- Add directions for scanning in the `Hint` and `More info` fields.

- Additionally, you should customize your `Run Button Label` to make it clear what command a user will be running within a survey. 

- Finally, as always, you can choose to make your script question private or required at the bottom of the builder page. You can also choose `Advanced` settings to implement things like relevance statements.

### Typical Scripts

- **Search the Question Set Library first!!**
Many specific tasks have already been created and designed by others - measuring nutrient density, soil carbon, or create custom calibrations.  Before creating your own survey, make sure what you're attempting to do hasn't been well designed by someone else that you can use!

- One simple "out-of-the-box" question set to start with is the **Simple Reflectometer Survey**. This question set includes the daily calibration process and three reflectometer scans and allows modification of the instructions so you can tailor the process to your projects needs.

#### Run a basic measurement
- **Use**: any general object, specialty calibrations, etc.
- In SurveyStack.io, create a new or open an existing survey.
- Select a Script question type
- Select `Reflectometer` script in the dropdown
- In `Parameters` box, paste in: `{"id":"standard-6","remove":["spad_","three_st"]}`
- **Result**: This will run a normal measurement, show and save values for each of the 10 wavelengths.

#### Run a measurement **for soil**
- **Use**: soil only
- In SurveyStack.io, create a new or open an existing survey.
- Select a Script question type
- Select `Reflectometer` script in the dropdown
- In `Parameters` box, paste in: `{"id":"our-sci-devices","protocol":"standardSoil","remove":["spad_"]}`
- **Result**: This setting applies a normalization factor for each wavelength of the device and is meant to reduce device-to-device variability to produce more accurate soil carbon models

#### Run a measurement **for leaves**
- **Use**: Leaves only
- In SurveyStack.io, create a new or open an existing survey.
- Select a Script question type
- Select `Reflectometer` script in the dropdown
- In `Parameters` box, paste in: `{"id":"standard-6", "leaf": 1, "remove":["spad_","three_st"]}`
- **Result**: This has all the data from a normal measurement, but also outputs for `ndvi`, `cvi`, `chlorophyll`, `carotenoids`, and `anthocyanins`.

### Meter-Specific Scripts & Parameters

**Reflectometer is the best script for the Meter. All adjustments to the script are managed by using parameters in SurveyStack.**
> - Meter scripts are stored as .zip files in GitLab, [here](https://gitlab.com/our-sci/software/scripts).

**Here is the list of reflectometer script parameters:**
> - calibrateTeflon
> - calibrateOpen
> - calibrateBlank
> - standardStress
> - standardWarmup
> - calibrateTeflonMaster
> - calibrateOpenMaster
> - testIntensity
> - testIntensityIr
> - standard
> - standardBlank
> - standardRaw
> - standardRawNoHeat
> - standardRawHeatCal
> - standardHeatTest
> - standardApplyCalibration
> - standardSoil
> - calibrateBlankLocal
> - environmental_only
> - resetAll
> - configure_bluetooth
> - configure_bluetooth_115200
> - set_device_info
> - print_memory

## SurveyStack Resources

In addition to meter-specific scripts for hardware-integrated surveys, you may find more general SurveyStack tutorials useful. You can find out more about creating and building surveys, as well as all SurveyStack question types below.

<details>

<br>
<summary><b>SurveyStack Tutorial: Create a New Survey</b></summary>

<iframe class="embedded-content" height = 3150px src="https://our-sci.gitlab.io/software/surveystack_tutorials/create_survey_quick/"></iframe>

</details>

<br>

<details>

<br>
<summary><b>SurveyStack Tutorial: Tour the Survey Builder</b></summary>

<iframe class="embedded-content" height = 1300px src="https://our-sci.gitlab.io/software/surveystack_tutorials/building_surveys/"></iframe>

</details>

<br>

<details>

<br>
<summary><b>SurveyStack Tutorial: Question Types</b></summary>

<iframe class="embedded-content" height = 10250px src="https://our-sci.gitlab.io/software/surveystack_tutorials/question_types/"></iframe>

</details>

<br>
