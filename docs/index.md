# Overview

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

Our-Sci, the Bionutrient Food Association, and the Bionutrient Institute are working in collaboration to build a community of meter users. This meter is a handheld tool that uses ten LEDs to emit ten discrete wavelengths, measures the reflectance of a sample, and correlates those measurements with data models to make functional predictions. 

**The meter is sold under the following names, but will be referred to simply as "the meter" in all documentation:**
> - Our Sci Reflectometer 
> - Quick Carbon Reflectometer 
> - Bionutrient Meter

**Applications for the Our Sci Reflectometer include:**
> - [Estimating nutritional outcomes in food](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/nutrient_density/)
> - [Predicting soil carbon](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/SOC/)
> - Measuring chlorophyll in leaves
> - [Build your own model](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/build_your_own_model/)

 <img src="https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/meter.png" width="300" height="300">

**The hardware is designed to be used in the field or in the lab for the following:** 
> - flat samples, such as leaves 
> - samples in cuvettes - for lab applications 
> - loose solids, such as soils, in a glass petri dish 

While it is possible to measure bulk solids such as fruits, the current design is less optimal for measuring the surface of a large solid object. 

**A more technical overview:**

The meter is a ten wavelength colorimeter with ten LEDs ranging from 365nm to 940nm, and two pin photodiode detectors with ranges from 350nm to 700nm, and 700nm to 1000nm respectively.  We use two separate detectors because shining visible light on samples containing chlorophyll will produce fluorescence in the 700 - 800nm range which will confound the reflectance signals. By having two separate detectors, we can separate the reflectance from the fluorescence. In addition, the LEDs are pulsed in order to save energy and lower heat accumulation (which produces noise). This allows for brighter flashes and therefore greater signal. Software subtraction of ambient light is used to deal with light contamination on the detectors.

### What is it? / What can it do?

<details>

<br>
<summary><b>Motivation Behind the Meter</b></summary>

<p>
While there are other spectrometers on the market, they tend to either be expensive full-spectrum tools or basic DIY kits - neither of which suits the precise needs of our partners. 
</p>

<p>
Like the <a href="https://www.thestar.com/news/insight/2016/01/16/when-us-air-force-discovered-the-flaw-of-averages.html">seat designed for the average person but usable by no one</a>, product designers should avoid the law of averages.  As is the case, the similar devices to our meter are too general purpose to be particularly useful. For <a href="http://blog.our-sci.net/people/">our community partners</a>, none of these devices do exactly what they need, which is…
 </p>

<ul>
<li>Arborists need a low-cost and easy-to-use chlorophyll meter to add more rigorous sensor data to visual tree assessments.</li>
<li>Consumers and farmers need a way to measure food nutrient density in stores and on farms.</li>
<li>Soil scientists and regulators need to measure soil carbon in the field, quickly and easily.  Doing so could create a massive new pathway for carbon markets to value sequestration of carbon in soil.</li>
<li>Cannabis growers, consumers, and dispensaries need to be able to confirm total cannabinoids and THC levels to comply with regulations, ensure quality product, and identify fraud.</li>
</ul>

<p>
These cases require a device which is low cost, easy to use by non-scientists, flexible in what it measures (drops of liquids, cuvettes, leaves, aggregate solids like soil, and whole solids like a pear), usable in field conditions, fast, and open source.  
</p>

</details>

<details>

<br>
<summary><b>Our Meter Design</b></summary>

<p>
Our core design uses LED-supplied light sources at ten different wavelengths. It is based on the open source MultispeQ, a photosynthesis measurement device, but is much lower cost. The meter is a pared-down version of this device with a less expensive bill of materials, fewer maintenance costs, and faster calibration and assembly times. It also has the advantage of working independent of ambient light, unlike a normal spectrometer or simple colorimeter, for which the sample must be analyzed in darkness.
</p>

<p>
Ideally, we want users to be able to measure soil carbon, leaf chlorophyll content, brix from extracted sap, and the density of a pear fruit all at the same time with the same instrument. This not only reduces the cost and increases utility, it also spreads our development costs across multiple applications. Our design accommodates all of these uses.
</p>

</details>

<br>

### Crowd-Sourcing Data

<details>

<br>
<summary><b>Purpose of Beta Launch</b></summary>

<p>
This device was engineered to be a tool with a wide range of uses, some of which have not been studied yet. In order to be accurate for its intended use, it requires reference data for use-specific prediction models.
</p>

<p>
Reflectance is a pretty simple measurement and tells you almost nothing without reference data. Reference data measures reflectance values and validated lab-based measurements on the same set of samples to build correlations between the two (if they exist!). But building a reference database can be very expensive. In the case of food nutrition, measuring a small suite of lab tests for vitamins, minerals and antioxidants can cost $500 or more. A reference database might contain hundreds or thousands of measurements to have sufficient predictive power. 
</p>

<p>
As a community-oriented organization, we are looking for a community-driven solution to develop use-specific prediction models. Right now, we’re looking for help in creating our reference data sets. We need users who are willing to collect data to build out these data sets on a broad enough scale to be useful for predictive models. These models will get our device to a stage appropriate for commercial use. 
</p>

</details>

<details>

<br>
<summary><b>Tested Device Capabilities</b></summary>

<p>
Out of the box, this device can measure chlorophyll content in plants.
</p>

<p>
With predictive modeling, we have this seen that this device can measure: 
</p>

<ul>
<li>Milkfat content in milk</li>
<li>Starch levels in produce</li>
<li>Soil organic carbon (SOC) in soil
    <ul>
    <li>We're currently conducting a pilot program to create local calibration models for soil carbon</li>
    </ul>
    </li>
<li>Relative nutritional content in food 
    <ul>We have predictive models ready for consumer use with the following 10 crops!
    <li><b>Beets:</b> Antioxidants</li>
    <li><b>Bok Choy:</b> Antioxidants; Brix; Polyphenols</li>
    <li><b>Carrots:</b> Polyphenols</li>
    <li><b>Kale:</b> Brix; Polyphenols</li>
    <li><b>Lettuce:</b> Antioxidants; Polyphenols</li>
    <li><b>Mustard Greens:</b> Antioxidants; Brix; Polyphenols</li>
    <li><b>Oats:</b> Antioxidants; BQI; Polyphenols</li>
    <li><b>Squash:</b> Polyphenols</li>
    <li><b>Swiss Chard:<b> Antioxidants; Brix</li>
    <li><b>Wheat: </b> Antioxidants; BQI; Polyphenols; Proteins</li>
    <ul>More coming soon!</ul>
    </ul>
    </li>
</ul>

<p>
Got other ideas? Great! The possibilities are wide open. We’ve set up <a href= "http://forum.goatech.org/c/our-sci/8">forums</a> for the community to discuss their thoughts, discoveries, challenges, and insights.
</p>

</details>

<br>

### Our Mission

<details>

<summary><b>Our-Sci's Broad Mission</b></summary>

<br>
<p>
We strive to support the development of research capacity in communities through software, hardware, and training.  Specifically, we want to help turn citizens into researchers, researchers into community leaders, and communities into solvers.
</p>

<p>
With our low-cost tools, we aim to:
</p>

<ul>
<li>Help communities disrupt the systems that they think need disruption</li>
<li>Improve data accessibility across a diverse set of users</li>
<li>Increase the scientific rigor of community-based projects and decision-making</li>
<li>Foster a network of communication, trust, and independence</li>
<li>Be transparent and open source in everything we do</li>

</details>
