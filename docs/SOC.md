<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 


# Predicting Soil Carbon FAQ


## Why use the meter to measure soil organic carbon?
Many different communities are interested in developing low-cost methods of measuring or predicting soil organic carbon (SOC):

- **Farmers, farmer education groups,** and **research organizations** want to improve soil health, productivity, and resilience.
- **Policy makers** and **ecosystem services markets** want to create value for soil carbon sequestration in soil.

## Has this meter shown an ability to predict soil organic carbon?
Yes, The Our Sci Reflectometer has been shown to provide precise and actionable soil carbon estimates in southern Africa: [*Accessible, affordable, fine-scale estimates of soil carbon for sustainable management in sub-Saharan Africa*](https://acsess.onlinelibrary.wiley.com/doi/10.1002/saj2.20263)

## Developing soil organic carbon (SOC) prediction models?
Currently, the only way to predict SOC with the Reflectometer is to develop your own SOC model and code it into a SurveyStack script.  To build your own soil carbon models you will need a training set of soil samples for which you have the following data:

-	Reflectance scans
-	Lab-derived soil carbon values
-	(optional) Slope and soil texture classes can improve soil carbon predictions and can be generated in the field using visual estimation and hand-texturing techniques.

Ideally, the training set should have at least 300 samples, though more samples may be needed depending on the variability of the soil conditions where you wish to predict soil carbon.

Our Sci is working with numerous partners to develop and deploy SOC prediction models using the Reflectometer. Email us at info@our-sci.net to learn more.

## Are there any public soil organic carbon (SOC) prediction models available?
Not yet, but we're working on it.

Over the past two years [Our Sci](https://www.our-sci.net/), the [Bionutrient Institute](https://www.bionutrientinstitute.org/), and [OpenTEAM](https://openteam.community/) hub farms have used the meter to capture spectral data from thousands of soils across the USA. Additionally, we have lab SOC data (LOI-C data from the Bionutrient Institute lab or dry combustion carbon from commercial labs) and other metadata such as GPS location for many of those soil samples.

In the first half of 2022, we will pull all this data together into a single dataset for use in developing SOC prediction models. Additionally, we will use publicly available data sets to pull in soil properties that impact the soils' ability to store carbon (ex: clay content, slope, etc.) and use this data to further improve our SOC prediction models. 

We are also exploring collaboration with the [Open Soil Spectroscopy Library](https://soilspectroscopy.org/) to use their modelling tools to develop and deploy SOC predictions.

## How can I get involved?
By building a large soil spectral library with a rich diversity of soil types and managements, we can build better, more robust models.

### Are you interested in contributing soil data to the library?
If so, the data that can best help us build SOC models are:

-	Meter scans
-	Lab derived SOC values, along with the method of determining SOC
-	Location metadata. While we are building an open data library for the meter, GPS coordinates and other personally identifiable data will be kept anonymized.

### Do you have modelling experience?
-	We are looking for collaborators with modelling experience that can help us test different SOC modelling approaches. 
-	We are also interested in comparing global models to hyperlocal models. 

Contact us at info@our-sci.net if you are interested in collaborating.

## Are you working on anything else related to soil carbon?
Yes, we are developing [SoilStack](https://www.our-sci.net/soilstack/) for smarter sampling. SoilStack uses publicly available datasets (NDVI, topography, soil properties) to stratify fields and optimize the locations for collecting soil samples based on in-field variability. Then, SoilStack guides users to each sampling location and collects key sampling metadata (GPS coordinates, timestamp, QR code scans and soil sample bag IDs). 

Currently, the [Ecosystem Services Marketplace Consortium](https://ecosystemservicesmarket.org/) in the USA and Carbofarm in Europe are using SoilStack to direct and manage soil sampling activities related to soil carbon sequestration projects. 

We are actively adding features to SoilStack throughout 2022, and by the end of the year users should be able to use the meter in the field through the SoilStack app.
