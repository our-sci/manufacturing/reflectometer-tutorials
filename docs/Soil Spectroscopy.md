# Soil Spectroscopy Study

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

## Introduction
The Bionutrient Institute lab receives soil samples along with the produce samples from our farm partners. Organic carbon and organic matter are measured for each of the samples using loss on ignition (LOI) and soil respiration tests.[^1] Along with these tests each of the soils are scanned with the Our Sci reflectometer.[^2] The goal of this process is to contribute to the creation of a soil organic carbon prediction model. A good model would allow anyone with a reflectometer to scan their dried soil and get an estimate of the organic carbon, without having to send their soil in for testing, saving time and money.

[^1]: [https://our-sci.gitlab.io/bionutrient-institute/bi-docs/](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/)  
[^2]:[https://www.our-sci.net/reflectometer/](https://www.our-sci.net/reflectometer/)

## Methods
Soils are sent to the lab in small plastic bags labeled with sample numbers. After being dried and sieved, a small scoop of soil  is transferred to a glass petri dish that fits over the light guide on the reflectometer and is scanned. After each soil sample the petri dish and scoop have to be cleaned. This can be a long process and in an effort to reduce the amount of time spent on this task we wanted to know if we could leave the soils in the ziploc bags for the scans instead of transferring a portion to the glass dishes.  
This experiment was conducted in two parts. During the first part we tested multiple different types of bags/containers: quart size ziploc, snack size ziploc, xrf bag, and the glass dish. There were 5 of each type of container. Each container was scanned on it’s own and then with 5 soils.  
In the second part of this experiment we looked at how different cleaning methods affected the scans when using a glass dish. Four soils from the same region with varying colors were selected. They will be referred to as A, B, C, and D in this report. One glass petri dish was used throughout the entire testing protocol.  Each soil was scanned and in between the scans the dish was cleaned using the following order of methods:
- Thoroughly cleaned with DI water  
- Blowing the dust out  
- Blowing dust out a second time  
- Kimwipe  
- Kimwipe a second time  
The four soils were scanned in a random order for each of the tests.

## Results
### Experiment 1
Each of the bags tested produced fairly similar results in comparison to one another (see figure 1). However, those results were not consistently different from the glass dish and soil measurement. At some wavelengths the bags produce spectral results above the glass dish and soil and at other wavelengths the bag results are consistently below the glass dish and soil measurement. 

**Figure 1.** Spectral response for each type of container at 10 wavelengths.  
![Figure 1](https://gitlab.com/our-sci/resources/-/raw/master/images/Spectroscopy%20Report/Figure%201.png)

Additionally, the glass dish and soil consistently produces a smaller standard deviation than the quart bag with soil and the snack ziploc and soil (see figure 2). 

**Figure 2.** Standard deviation for each container type  
![Figure 2](https://gitlab.com/our-sci/resources/-/raw/master/images/Spectroscopy%20Report/Figure%202.png)

Experiment 2
We averaged the spectral results for each cleaning method and found the standard deviation was quite large for each of the soils (see figure 3).

**Figure 3.** Standard deviation between cleaning methods for each soil  
![Figure 3](https://gitlab.com/our-sci/resources/-/raw/master/images/Spectroscopy%20Report/Figure%203.png)

We also compared the standard deviation of the air cleaning and kimwipe cleaning methods for each of the soils (see figure 4). The difference in standard deviation between the methods varies from soil to soil.  

**Figure 4.** Standard deviation for air and kimwipe cleaning methods  
![Figure 4](https://gitlab.com/our-sci/resources/-/raw/master/images/Spectroscopy%20Report/Figure%204.png)

## Discussion
From experiment 1 we learned that there was consistently more variability in spectral response from the ziploc bags in comparison to the glass petri dishes which could impact the creation of a soil carbon model, especially if data from previous years is used. From experiment 2 we found that there was large variation between measurements with each cleaning method. When comparing air cleaning and kimwipe cleaning there was not one method that stood out as being consistently better. From that we can conclude that the cleaning method should stay the same, but either method could be used.
 
We also wanted to consider factors outside the lab that would influence our decision. From observation in previous years we have found that during shipping the ziplocs with soil tend to get beat up and the bags are sometimes damaged. This would lead to inconsistencies if we were to scan the soils through the bags. 

Due to shipping issues and the results of the study we decided to continue using the glass petri dishes with a consistent cleaning procedure and multiple scans for every soil. This will take some extra time, but it will produce the most accurate results.
