# Getting Started with Our Sci Instruments

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

<h3>Our Sci Instruments</h3>
<ul>
<li>Reflectometer (also known as the Bionutrient Meter)</li>
<li>SAVR Kit (Soil Assessment via Respiration)</li>
</ul>

<h2>Using Our Sci Instruments</h2>
Our Sci instruments connect to the <a href="https://www.surveystack.io/">SurveyStack</a> platform via an integration with the SurveyStack Kit app. </li>
<br>


<B>SurveyStack</B> is a flexible, open-source survey software designed to empower shared community knowledge. SurveyStack allows users to develop and deploy custom scripts within surveys. This provides a unique opportunity for users to both run sensors within surveys and to use the output of those measurements in other scripts. For example, users can deploy a model to predict soil carbon in SurveyStack that combines soil reflectance spectra output by the Reflectometer together with other survey data (soil type, slope, etc) to generate real-time estimates.
<br>

<B>SurveyStack kit</B> is a native android app, which manages the Bluetooth connection between instruments and SurveyStack. SurveyStack kit runs the measurements and pushes the results into SurveyStack, where they are accessible by other scripts within the same survey and get submitted to the SurveyStack database. 
<br>

The SurveyStack kit app only works on <B>android</B> devices (phones or tablets).


<h2>Setting up your android device</h2>
<ul>
<li>In the Google PlayStore, install the <a href="https://play.google.com/store/apps/details?id=io.surveystack.app">SurveyStack Kit App</a>.</li>
<li>If you anticipate using Our Sci instruments in area’s without internet connectivity, you should also <a href="https://our-sci.gitlab.io/software/surveystack_tutorials/navigation/#add-the-app-to-your-homescreen">add SurveyStack to your homescreen</a>.</.li>
<li>Create a SurveyStack account. If you are part of a SurveyStack group, a group administrator may create an account for you. Otherwise, you can register an account in <a href="https://app.surveystack.io/">SurveyStack</a> (or at <a href="https://bionutrient.surveystack.io/">bionutrient.surveystack.io</a>  if you are a BFA user).</li>
</ul>
  

<h2>Try it out</h2>
Use the <a href="https://app.surveystack.io/groups/5e95e368fbbf75000146a006/surveys/63cae4c4e64df800010ee477/submissions/new">Reflectometer Training Survey</a> to walk through the process of pairing your device, connecting it to the app and taking a measurement.
<br>
<br>
<h3>For more help, check out these pages:</h3>
<ul>
<li><a href="https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/connect_a_device/">Connect a device</a></li>
<li><a href="https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/range_calibration/">Calibrate your device</a></li>
</ul>
<h2>Need Help?</h2>
Reach out to hardware-support@our-sci.net for support over email, post your questions on the <a href="http://forum.goatech.org/">the Our Sci forum</a>, or schedule a meeting at a time that works for you.
<br>
<br>



<h2>Useful Pages within Documentation & Tutorials</h2>

<h3>Getting Started in SurveyStack</h3>
<ul>
<li><a href="https://our-sci.gitlab.io/software/surveystack_tutorials/">Welcome to SurveyStack</a></li>
<li><a href="https://our-sci.gitlab.io/software/surveystack_tutorials/navigation/">Navigating the App</a></li>
</ul>

<h3>Video tutorial for the Bionutrient Meter</h3>
<ul>
<li><a href="https://vimeo.com/662406087">How to set up your Bionutrient Meter</a></li>
</ul>

<h3>Getting Started with Forms & Surveys</h3>
<ul>
<li><a href="https://our-sci.gitlab.io/software/surveystack_tutorials/filling_out_surveys/">Completing a Survey</a></li>
<li><a href="https://our-sci.gitlab.io/software/surveystack_tutorials/view_results/">Viewing Results</a></li>
<li><a href="https://our-sci.gitlab.io/software/surveystack_tutorials/create_new_survey/">Creating a Survey</a></li>
</ul>

<h3>Getting Started with your Meter</h3>
<ul>
<li><a href="https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/">Device Overview</a></li>
<li><a href="https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/app_expectations/">What to Expect for Reflectometer Measurements</a></li>
<li><a href="https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/reflectometer_scripts/">Writing Surveys for Reflectometers Measurements</a></li>

</ul>
