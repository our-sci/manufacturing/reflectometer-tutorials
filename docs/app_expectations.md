# What to Expect with Hardware Integrations

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

Again, the SurveyStack app (or online platform) requires the SurveyStack Kit app to manage hardware integrations. As a result, SurveyStack and the SurveyStack Kit app are built to toggle between each other as needed. Here is the standard process to expect when running hardware-integrated measurements:

<h4>Assign App Preferences for Scans</h4>

- The first time you run a measurement in SurveyStack (after [installing the SurveyStack Kit app](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/kit_installation/)), it will ask you where to open the script. You should select `SurveyStack Kit app,` followed by `ALWAYS` so that SurveyStack knows this is your preference every time.
> - Note, the APK install button for SurveyStack Kit will always display on script-type questions for which the "Native Script" box is selected (for example, the "Warmup" script for the device calibration question set). If you have SurveyStack Kit installed, you can ignore that button. 

- After you've set up that preference stated above, whenever you click `RUN SCRIPT,` you will automatically jump from SurveyStack to the SurveyStack Kit app. 

<h4>Run Measurements in SurveyStack Kit</h4>

- Once in the SurveyStack Kit app, you will need to `Connect to Device` and then `RUN MEASUREMENT` to get your meter to scan. During a successful measurement, you will see multi-colored LEDs flashing beneath the reflectometer's light guide. 
> - If you do not know how to connect your device, check out [this tutorial page](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/connect_a_device/).
> - For best performance, you should be sure to warm up and [Calibrate Your Device](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/range_calibration/) each time you power on the device, or at least at the beginning of each day, as well as after a significant weather change.

<h4>After Completing the Scan</h4>

- After completing the measurement, the SurveyStack Kit app will display the results. The results displayed in the app are the raw spectral results, which you likely have little need to evaluate. After running a scan and reaching the results page, just select `DONE` at the bottom of the page to return to the SurveyStack form in which the measurement was embedded. 
> - If you would like to redo the measurement for any reason, simply select `RE-RUN` so that you can return the the `RUN MEASUREMENT` screen.
- **Once back in SurveyStack, make sure that the results from your scan are displayed. You will also see either a `Success` or `Error` banner at the top of the page. You should redo the measurement if you see an `Error.`** 
> - If there is a spinning circle or simply no results displayed, you may need to rerun the script to ensure that the data is saved.
> > - If after 20-30 seconds this screen still hasn't reported a result, you may need to refresh the page and run the measurement again. Make sure you are connected to wifi and your device is connected in SurveyStack Kit, then run again.

