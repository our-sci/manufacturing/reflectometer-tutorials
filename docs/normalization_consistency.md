# Normalization based on sample “matrix”  
## Soil petri dish, 5ml cuvette, 1ml cuvette  

### Motivation  
The Reflectometer has shown very low levels of variability in sampling a single sample on a single device.  The standard deviation of 15 devices is generally between 0 - 1% in those cases, depending on the LED, with many LEDs below 0.5%.  However, variation between devices has been higher - 3.5 - 8% or higher generally depending on the LED, even though we have a normalization process in place to a flat PETG calibration stick for all devices.  Some of this is caused by physical variation in each device around the light guide and lights, causing the level of reflection versus refraction to vary across devices based on the sample type.  In short, samples which are very different in how they scatter (or not scatter) light from our PETG calibration stick benefit less from the current normalization process.

We performed tests to determine if we could add other normalization options for commonly used sample setups, or sample “matrices”, in order to further reduce device to device variation.  

*Background on the current process* 

*The existing measurement and calibration math on a given reading consists of several steps:*

1. ***Internal averaging and ‘leveling’ of signal -*** *this is done on measurements all the time.  60 samples are taken (15us pulse, 1500us apart), 30 are removed (these are heavily tailed due to heat), remaining 30 have outliers removed and then are flattened (slope identified, normalized to slope = 0).  A median is applied to the final output.*  
2. ***A heat calibration correction is applied.***   *The heat calibration is calculated across a range of temperatures, but in fact is measured as a range of voltages on each LED (voltage relates directly to temperature).  The relationships are calculated as a 2nd order polynomial and values saved to the device.  Raw ‘leveled’ results (from (1) above) have this 2nd order poly applied.  Generally speaking, colder LEDs have high signal, while hotter LEDs have lower signal.*  
3. ***A range calibration correction is applied.***  *Taking the output of heat calibration corrected reading, the reading is then normalized between the low value (‘blank’ measurement, lowest possible signal) and high value (petg or baso4 calibration sticks, highest possible signal).  These low and high values are set as 0 and 10,000 respectively.  The calculation applies the range calibration of the users calibration stick, however, that stick is also referenced to the factory calibration stick, so (in theory!) all calibration sticks are equal (similar to NIST traceability to a common ‘master’ calibration item).*  

### Description  
Across 9 devices, we measured all devices against a set of samples 18 samples.  In prior work, we identified that 5ml Cuvettes (plastic), 1ml Cuvettes (glass), and Petri dishes (glass) with soil were unique matrices and commonly measured items among Reflectometer users, thus worth creating normalizations to.

After collecting the data on each device x sample pair (run in triplicate and averaged), the data was cleaned.  A few outliers were removed (appropriate in this case).  Then…

1. An average for each sample was calculated, combining measurements from all devices.  
2. Several samples were selected to normalize against.  Some samples were better to normalize against because they were more representative of a ‘typical’ sample of that matrix.  For example: a light to medium dilution of water in a 5ml cuvette produces a more effective normalization than a very dense water solution.  Since most samples used by most users will be fairly dilute, we selected the light to medium dilution for normalization.  To test effectiveness the following is performed for each wavelength:  
  a. An = average value across all devices for the normalized sample  
  b. Ad = average value for a single device for the normalized sample  
  c. N = An / Ad = normalization factor for a single device  
  d. Sd = value for a new sample on a single device  
  e. Sn = Sd * N = normalized value for a new sample on a single device  
  f. Test effectiveness  
    - For a given sample, look at the standard deviation across all devices for a single sample (Sd across many devices).  
    - For the same sample, look at the standard deviation across all devices for a single sample (Sn across many devices).  
    - Calculate the reduction of the standard deviation of all Sn’s versus all Sd’s.  
3. Once the most effective sample was selected, An is saved in the “Normalization and QC” script used for device manufacturing in order to inform N which is saved to the device in the EEPROM (device memory).  Devices must be upgraded to firmware version 4.0.6 (go here to update)  
4. Once saved to the device, these normalizations can be applied in the future by changing the protocol variable in the script in the following way  
  a. Normal measurement: { “id”: “our-sci-devices”, “protocol”: “standard” }  
  b. Soil normalized:  { “id”: “our-sci-devices”, “protocol”: “soilStandard” }  
  c. 5ml Cuvette normalized:  { “id”: “our-sci-devices”, “protocol”: “5mlcuvetteStandard” }  
  d. 1ml Cuvette normalized:  { “id”: “our-sci-devices”, “protocol”: “1mlcuvetteStandard” }  
5. Finally, the normalization is most effective in a range of intensities.  So for each, the recommended intensity range is shown.  Normalization does not increase variation, but at intensities outside the normalized range there will not be a reduction in variation.  

### Soil  
![Standard Deviation Reduction](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/soil_1.png)  
*Shows the average reduction in standard deviation across UV, VIS and NIR LEDs both in raw (absolute value) and as % of the median.  Also shows the efficacy of 3 different potential samples to normalize against.  The one selected was the one highlighted in bold.  The reduction is also shown as a factor of the original by UV, VIS, and NIR.*  

![Standard Deviation Comparison 1](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/soil_2.png)  
*Comparison of standard deviation across 19 sample and 10 wavelengths.  The sample IDs are on the left side.  You can see an improvement across the board - standard deviation decreased at all wavelengths across all samples.*  

### 1ml Cuvette  
![Standard Deviation Reduction 2](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/cuvette_1.png) 
*Shows the average reduction in standard deviation across UV, VIS and NIR LEDs both in raw (absolute value) and as % of the median.  Also shows the efficacy of 3 different potential samples to normalize against.  The one selected was the one highlighted in bold.  The reduction is also shown as a factor of the original by UV, VIS, and NIR.*  

![Standard Deviation Comparison 2](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/cuvette_2.png)  
*Comparison of standard deviation across 18 sample and 10 wavelengths.  The sample IDs are on the left side.  Generally, higher concentration samples (sample 5 for black, and 3 for the other wavelengths) had more dense color and the normalization did see improvements over the dense colors.  However, for lighter densities the improvement was consistent, as well as for colors which didn’t change much (like NIR and UV).*  

![5ml cuvette](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/cuvette_3.png)  
*This shows the reflectance levels that the normalization is most effective.  This does not prevent use at other reflectance levels, but normalization will be most effective in the ranges stated here.*  

### 5ml Cuvette  
![Standard Deviation Reduction 3](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/cuvette_4.png)  
*Shows the average reduction in standard deviation across UV, VIS and NIR LEDs both in raw (absolute value) and as % of the median.  Also shows the efficacy of 3 different potential samples to normalize against.  The one selected was the one highlighted in bold.  The reduction is also shown as a factor of the original by UV, VIS, and NIR.*  

![Standard Deviation Comparison 3](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/cuvette_5.png)  
*Comparison of standard deviation across 18 sample and 10 wavelengths.  The sample IDs are on the left side.  Generally, higher concentration samples (sample 5 for black, and 3 for the other wavelengths) had more dense color and the normalization did see improvements over the dense colors.  However, for lighter densities the improvement was consistent, as well as for colors which didn’t change much (like NIR and UV).  In general, the improvement was lowest for the 5ml cuvette compared to all other cases, and primarily benefited the UV and NIR ranges.*  

**Blanks**  
In addition, a set of blank plastic cuvettes were run to determine if the normalization helped improve blanks as well.  This is important because noisy blanks will add noise to the actual sample.  In general, **blank 5ml cuvettes showed basically no improvement from the normalized versus non-normalized.**  This isn’t a huge surprise given that the water sample did not improve the 5ml cuvette normalization either.  

![5ml cuvette 2](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/blanks_1.png)  
*This shows the reflectance levels that the normalization is most effective.  This does not prevent use at other reflectance levels, but normalization will be most effective in the ranges stated here.*  

### Conclusions  
Overall it’s clear that the normalization procedure has a significant benefit in reducing device to device variation depending on the matrix and situation.  Generally for soil samples, there is a reduction across the board (many samples, all wavelengths), while the reduction for cuvettes is best for the 1ml glass cuvettes and mainly in lower concentrations (less color).  

Reference: final saved values

The saved reference values for each device are shown below.  These are entered in the “Normalization and QC” script in the “params” section for each normalization.

**Soil**  
![soil conclusion](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/conclusion_1.JPG)  

**1ml Cuvette**  
![1ml cuvette conclusion](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/conclusion_2.JPG)  

**5ml Cuvette**  
![5ml cuvette conclusion](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/normalization_consistency/conclusion_3.JPG)



