# Technical Reports

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

We're always working to optimize the accuracy and consistency of our devices. Below is a summary of key findings and technical discoveries from research, experimentation, and testing with 30 of Meters.


<iframe class="embedded-content" height=3500px src="https://octavioduarte.gitlab.io/public-briefs/deviceConsistency"></iframe>
