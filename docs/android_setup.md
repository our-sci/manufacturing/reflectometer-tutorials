# Getting Started with an Android

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

<h3>Video Instructions</h3>
<iframe width="560" height="315" src="https://www.youtube.com/embed/pTpuqH7D9ek" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
_This video covers Android phone setup. If you ordered an Android tablet with your meter, you can skip this step, because all apps are already pre-installed._

<h2>Join Our Weekly Community Call</h2>
<p>
<strong>We know that getting started with new technology can be overwhelming. If you’ve received your device and are not sure how to begin, or would just like to join a call with other users to share tips and discuss use cases, check out our weekly community call. We are also happy to help you set up your Android in this call!</strong>
</p>

<p>
<strong>You can join at <a href="https://meet.google.com/ntp-ukyh-rbx">this Google Meet link</a> or dial ‪(US) +1 413-648-7923‬ PIN: ‪921 538 870‬#. For international folks, more phone numbers can be found <a href="https://tel.meet/ntp-ukyh-rbx?pin=3456940824572">here</a>. We will meet each Tuesday from 11 am - 12 pm EST.</strong>
</p>
<p>
If you can't attend at this time, feel free to post your questions on <a href="http://forum.goatech.org/">the Our Sci forum</a>. This will make sure we can find the best person to support you! 
</p>

<p>
Or, reach out to hardware-support@our-sci.net to ask your question over email, or schedule a meeting at a time that works for you.
</p>
