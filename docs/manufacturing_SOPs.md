# Manufacturing SOPs

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

All of our products are open source. To build this devices yourself, you will need the following documentation:

- [Firmware](https://gitlab.com/our-sci/reflectance-spec-firmware)

- [PCB](https://gitlab.com/our-sci/manufacturing/reflectance-spec-PCB)

- [CAD / Physical design](https://cad.onshape.com/documents/849be056da41993fee5440bf/w/5ea0968e39e13e6e3de3a5ba/e/f5bcf0fa9d4cdfc9de388d42)

- [Bill of Materials (everything but the PCB)](https://docs.google.com/spreadsheets/d/1d4Xplr8sztEgrEis-LiSqLnN9V1eaN1zSc19izoaaig/edit?usp=sharing)

Additionally, we have built standard operating procedures (SOPs) for each step of in-house device assembly. Please note, these SOPs are subject to change as we continue to optimize the Our Sci Reflectometer. 

If you plan to construct your own Our Sci Reflectometer, we suggest that you reference these materials, and get in touch with us on [our forum](http://forum.goatech.org/c/our-sci/8) or via email at hardware-support@our-sci.net.

**Case Preparation:**
> - [Case Prep 1 - Preparing 3D Printed Parts](https://app.surveystack.io/surveys/602bd8bf04aa9e00010188d2)
> - [Case Prep 2 - Preparing Clamps](https://app.surveystack.io/surveys/603cf04a15b7d00001d89942)
> - [ Case Prep 3 - Securing Clamps to Case Bodies](https://app.surveystack.io/surveys/6033b651c38a9a0001a0772f)

**Electronics Preparation:**
> - [Electronics Prep 1 - Soldering Bluetooth](https://app.surveystack.io/surveys/6054b0315244b90001b5e1ce)
> - [Electronics Prep 2 - Soldering Detectors and Hot Gluing Batteries](https://app.surveystack.io/surveys/60423f3b2757bf0001abfe9c)
> - [Electronics Prep 3 - Gluing Optical Filters and UV Curing](https://app.surveystack.io/surveys/604f91110d39700001c98bb1)

**Device Assembly:**
> - [Light Guides - Laser Cutting](https://app.surveystack.io/surveys/602e7a053b6e61000175c54c)
> - [Device Assembly - Installing Circuit Boards in Cases](https://app.surveystack.io/surveys/605dd9f0bb47f200016a3064)
> - Packing Cuvette Standards - Coming Soon! 

**Device QAQC & Calibration:**
> - [Device Manufacture 2](https://app.surveystack.io/surveys/61ae8d0341ba590001710ef6)
- - Used to check detectors, assign a device ID, and configure bluetooth.
- - Will be transferred from the Our SciKit Legacy app to SurveyStack in the coming months.
> - [Heat Calibration](https://app.surveystack.io/surveys/61d318ceecef660001bc409e)
- - Calibrates the LEDs and detectors across a range of expected natural temperatures.
- - Will be transferred from the Our SciKit Legacy app to SurveyStack in the coming months.
> - [Firmware Update](https://app.surveystack.io/surveys/60f2f2e171e7580001d68f81)
- - Usually, the circuit board manufacturer will upload the correct firmware to the board before shipping to a local manufacturer. If there's a firmware change between batches though, you'll need to manually upload the updated version.
- - It has been transferred from the Our SciKit Legacy app to SurveyStack kit.
> - [Device Normalization and QC](https://app.surveystack.io/surveys/60b2da788bb0760001be45a9)
- - This is the manufacturer calibration that standardizes all devices to a single barium sulfate cuvette standard, and assigns a unique local standard to be shipped with each device.

**Kit Assembly:**
> - [Kit Prep 1 - Building and Packing Boxes](https://app.surveystack.io/surveys/6059f152481e6f00019a6574)
> - [Kit Prep 2 - Packing and Shipping](https://app.surveystack.io/surveys/605b6e8594951800010bab1a)
