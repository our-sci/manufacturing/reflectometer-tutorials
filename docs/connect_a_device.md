# Connect a Device

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5XDVDNEMDM');
</script>

**Important Note: your instrument can only connect to Android devices!**


### Turn on the Meter
  - The device must be turned on before it can connect to the SurveyStack Kit Android app
  - Press and hold the **Power button** for ~3 seconds
  - The green or blue **Power indicator light** will come on if the device is on
  - The device will automatically turn off after 5 minutes of inactivity

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/e5d3fbde4e1b9c3ff5b34e2b3a98ca70/image.png "labeled reflectometer")

### Pairing an device
Our Sci devices connect to your android using Bluetooth. The first time you connect your Our Sci device to SurveyStack Kit, you will need to **Pair** it to your android device. 

1. Go to you Android's `Bluetooth` or `Connections` settings
2. Search for available bluetooth devices. Ensure the Reflectometer is on.
3. Once your device appears in the list (the **Device ID** in the list will match the **Device ID** on your meter's lanyard), select `PAIR`. 
4. Select the device and enter in the code when prompted. The code for all devices is ***1234***
> - Note that when pairing with Bluetooth, the device name that you're looking for is likely similar but may not exactly match the ID# on your device's wrist strap. For example, a Device ID written as "02:5f" on the wrist strap may show up as "25f".
> - Bluetooth IDs will generally match exactly as written on the wrist strap, with the format "##:##".

  ***Note:***  For some phones, the **Pairing Device** pop up will appear in the phones `Notifications` as a `Pairing Request`. If the **Pairing Device** dialog box does not appear automatically, swipe down for the phone's notifications and tap on the `Pairing Request`.


### Connecting to an already paired device

Connecting is different from pairing the reflectometer. Once you’ve paired the reflectometer to your Android, you will not have to re-pair the device, however, you will have to reconnect the device every time the reflectometer turns back on.

You will need to connect your device when you reach the point in a survey to run a measurement. Follow these steps when you see the button “CONNECT TO DEVICE”.

1. If your Reflectometer is on, select `CONNECT TO DEVICE”`. This will connect your android to the device it was most recently connected to.
2. If this does not work, or you are trying to connect a different device to your android, select `DEVICE` from the lower right-hand corner of the app, this will take you to the list of Reflectometers that are paired to your android device.
3. Select the Reflectometer from the Device list and press `CONNECT`. If you do not see your Reflectometer in the list, you will need to `PAIR` it to your android device first.
4. Once your device is connected it will appear at the top of the device list.
5. To return to the measurement screen, select the `BACK` button.

![image](https://gitlab.com/our-sci/manufacturing/reflectometer-tutorials/-/raw/master/img/connect_to_device.png "Connect Device")

## Get Help

- Post your issues on the [Our Sci forum](http://forum.goatech.org/).  This will make sure we can find the best person to support you!

- Reach out to hardware-support@our-sci.net to ask your question over email, or schedule a meeting at a time that works for you.
